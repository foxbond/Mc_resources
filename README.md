MC Server info
===========================

Pamiętajmy, nie jestem administratorem ;) Może będę to aktualizował, może nie. Czasem raz na tydzień, czasem kilka razy dziennie - zobaczymy.
Repozytorium pozostaje publiczne i ogólnodostępne. Każdy kto potrafi obsługiwać GIT-a może wnosić poprawki.

* [Status serwera](https://dinnerbone.com/minecraft/tools/status/) *Należy podać IP, celowo pomijam*
* [Mapa](https://foxbond.gitlab.io/Mc_resources)
* [Działki](parcels.md)
* [Kolejka](metro.md)
* [Mapa biomów](http://mineatlas.com/?levelName=-7090927647366210652&seed=-7090927647366210652&mapCentreX=880&mapCentreY=-80&mapZoom=14&pos=&Player=true&Spawn=true&Likely+Villages=true&Ocean+Monuments=false&Jungle+Temples=false&Desert+Temples=false&Witch+Huts=false&Slime+Chunks=false)
* Seed: -7090927647366210652






*by [Foxbond](http://foxbond.info)*