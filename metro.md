# Kolejka


### Parametry linii: 
* Głębokość: 50 (y:50)
* Wymiary: 3x3 inner (5x5 outer). W razie potrzeby poszerzamy o parzystą ilość (5x3, 7x3, ...). 
* Tory zasilające umieszczane są po 3 w odstępach po 15 zwykłych torów. Pochodnie zasilające powinny znajdywać się po tej samej stronie na całej długości linii lub schowane pod podłogą.
* Oświetlenie: wystarczające ;)
* Styl: Raw stone 



### Parametry stacji:
Stacje znajdują się na terenach prywatnych, każdy robi jaka mu się podoba. Jedynym wymogiem jest umieszczenie **Ender Chest (Skrzynia kresu)** w widocznym miejscu. Styl stacji publicznych (umieszczonych w np. wioskach NPC) również jest wolą budującego :)
Ostatni raz automatyzowałem stacje kolejki w becie lub wcześniej dlatego temat pozostaje otwarty. Można robić dystrybutory wagoników, ale Ender Chest i tak musi się znaleźć.



### Parametry węzłów (Junctions, skrzyżowań):
Temat otwarty, mam pewien pomysł ale wymaga testów. Jeśli zadziała to będzie miodzio :3



### Uwagi:
* Głębokość 50 tyczy się odcinków "publicznych". W obrębie swojej posesji możemy mieć tory na jakim poziomie chcemy.
* W zależności od sytuacji (i wizji artystycznej ^^) odcinki ppubliczne mogą znajdywać się na innym poziomie lecz prawdopodobnie nie będzie się można do nich podłączać w takiej sytuacji. (Mosty, wiadukty, tunele podwodne)
* Odcinki prywatne mogą mieć dowolne wymiary i ksztalty. Dobrze by jednak było żeby ktoś nie dostał klaustrofobii w wyniku poruszania się tunelem 1x2 ;)
* Technika układania torów 3x15 została przeze mnie sprawdzona, prędkość jest maksymalna lub wagonik zwalnia nieznacznie. Dodatkowo, gdy ucieknie nam wagonik powinien zatrzymać się idealnie kratkę przed kolejnym przyspieszeniem :)
* Sposób zasilania kolejki na wzniesieniach nie został przeze mnie sprawdzony, ale tyczy się tylko odcinków prywatnych i sytuacji wyjątkowych więc można ustalać indywiduwalnie.
* Dowolność oświetlania, w przypadku odcinków wspólnych musi być identyczne na całej długości, prywatne - samowolka ;)
* Styl "Raw Stone"? Wtf? Wykorzystujemy bloki, które naturalnie występują pod ziemią (Stone, Granite, Diorite, Andesite), zastępujemy jedynie Dirt i Gravel. "Standardowe" oświetlenie to dwie pochodnie co 7 klocek. (6 odstępu)
* Ender Chest (Skrzynia Kresu)? Po co? Na każdej stacji powinna się znajdywać skrzynia kresu w której każdy gracz powinien mieć ze 2-3 wagoniki. W ten sposób nie trzeba się będzie martwić, że na jednej stacji jest ich 20, a na drugiej nie ma czym jechać.
* Standardem jest jedna nitka torów w tunelu. Jeśli wybrane odcinki będą szczególnie obciążone zostanie w nich dodana druga. Na odcinkach prywatnych można robić dowolną ilość.
* Jeśli w jednym tunelu jest więcej niż jedna nitka torów to te, które idą w przeciwległych kierunkach muszą mieć minimum jedną kratkę odstępu miedzy sobą.
* Jeszcze dziś (24.07) pojawi się w okolicy portalu do Netheru skrzynia (skrzynie) na datki. Proszę, PROSZĘ, jej nie okradać. Może i jestem, hehe, bogaty, ale skoro ktoś nie kopie tuneli to chociaż może wspomóc surowcami. Otrzymanych materiałów nie będę brał do siebie, każdy kto będzie pracował przy kolejce ma do nich dostęp.
* Jako datki przyjmuję: drewno (nie deski i nie patyki, szkoda miejsca), żelazo, złoto, redstone, węgiel (do pochodni, przetapiania rud metali), diamenty (naprawa narzędzi), kilofy/ksiązki Efficiency (Wydajność)/Unbreaking (Niezniszczalność) 4+ (Mam kilka, ale koszt naprawy szybko rośnie) i oczywiście tory (wszystkie rodzaje)
* Nie mam nic do kobla (Cobblestone), ale z umiarem, pls.
* Przydałby się Beacon przy budowie podziemnych stacji :)
