var mapSize = {
	width: 3000,
	height: 2000,
	windowWidth: 800,//$('#map').width(),
	windowHeight: 600//$('#map').height()
};
var currentSize = 1;

var curr;
$(document).ready(function(){
	
	$(".mapImage").draggable({
	  drag: function(ev, ui){
		  curr = $(ev.target);
		  curr.top = parseInt(curr.css('top'));
		  curr.left = parseInt(curr.css('left'));
		  
		  //lock up
		  if (curr.top > Math.abs(parseInt(curr.css('margin-top')))){
			  curr.top = Math.abs(parseInt(curr.css('margin-top')));
			  ui.position.top = curr.top;
		  }
		  
		  //lock left
		  if (curr.left > Math.abs(parseInt(curr.css('margin-left')))){
			  curr.left = Math.abs(parseInt(curr.css('margin-left')));
			  ui.position.left = curr.left;
		  }
		  
		  //lock bottom
		  var max = ($(".mapImage")[0].height*currentSize-Math.abs(parseInt(curr.css('margin-top')))-mapSize.windowHeight)*-1;
		  if (curr.top < max){
			  curr.top = max;
			  ui.position.top = curr.top;
		  }
		  
		  //lock right
		  var max = ($(".mapImage")[0].width*currentSize-Math.abs(parseInt(curr.css('margin-left')))-mapSize.windowWidth)*-1;
		  if (curr.left < max){
			  curr.left = max;
			  ui.position.left = curr.left;
		  }
		  
		  $(".mapImage").css({
			  'top':curr.top,
			  'left': curr.left
		  });
	  }
	});
	
	$(".buttonSize").click(function(ev){
		//recalculate top/left values instead of always auto-centering
		var newSize = parseInt($(ev.target).val());
		
		if (newSize == currentSize) return;
		
		var diff = newSize - currentSize;
		var mult = newSize / currentSize;
		var currTop = parseInt($(".mapImage").first().css('top'));
		var currLeft = parseInt($(".mapImage").first().css('left'));
		
		var point = {
			y: ((currTop +(300*(currentSize-1)))/currentSize),
			x: ((currLeft+(400*(currentSize-1)))/currentSize)
		};
		
		var newTop = (point.y)*newSize-(300*(newSize-1));
		var newLeft = (point.x)*newSize-(400*(newSize-1));
		
		/*
		console.log({
			currentSize:currentSize,
			newSize:newSize,
			mult:mult,
			diff:diff,
			currTop:currTop,
			currLeft:currLeft,
			newTop:newTop,
			newLeft:newLeft,
			point:point
		});
		*/
		$(".mapImage").css({
			top: newTop,
			left: newLeft,
			width: mapSize.width*newSize,
			height: mapSize.height*newSize
		});
		
		//centerMap();
		currentSize = newSize;
	});
	
	$("#centerMap").click(centerMap);
	
	$("#alignMap").click(alignMap);
	
	$(".optVisibility").change(function(ev){
		
		$("#mapImage_"+$(ev.target).val()).css('display', ($(ev.target).prop('checked') ? 'block' : 'none'));
		
		//console.log(ev, );
	});
});


function centerMap (){
	$(".mapImage").css({//lets make it dynamic ;p
		'top': -1000*currentSize,
		'left': -1500*currentSize
	});
}

function alignMap () {
	$(".mapImage").css({
		'top':$(".mapImage").first().css('top'),
		'left': $(".mapImage").first().css('left')
	});
}